package br.com.bagulhosibreguetes.servico;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.bagulhosibreguetes.modelo.Usuario;

@Path("/fachada-servico")
public class FachadaServico {

	@GET
	@Path("/usuario")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Usuario usuario() {
		Usuario u = new Usuario();
		u.setId((long) 1);
		u.setNome("Filipe Costa");
		u.setEmail("flp.costas@gmail.com");
		u.setSenha("pass");
		return u;
	}
}
