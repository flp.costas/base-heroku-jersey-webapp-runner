# Modelo projeto Heroku - deploy Tomcat Jersey/JAX-RS

Esse � um modelo de aplica��o webapp para ser criado no heroku.
Utiliza o webapp-runner, um jar para executar um pacote WAR no heroku.
O deploy � simples e r�pido.

Basta efetuar um fork do projeto e come�ar a trabalhar.

## Webapp Runner
Arquetipo utilizado:


	:::term
	mvn archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DarchetypeArtifactId=maven-archetype-webapp -Dversion=1.0-SNAPSHOT -DgroupId=br.fcsilva.modelo -DartifactId=heroku-webapp-runner -DinteractiveMode=false




